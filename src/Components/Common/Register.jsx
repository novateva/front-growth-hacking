import React, { Component, memo } from 'react';
import { Form, Icon, Input, Button, Typography, Row, Col, Select, Divider } from 'antd';

import Logo from '../../img/logo.png';
import { CountryPhones, countryNames } from './constants';

const { Text } = Typography;
const { Option } = Select;

class RegisterFormBase extends Component {
  state = { initialCountry: '', CountryOptions: [] };

  componentDidMount() {
    let initialCountry;
    const CountryOptions = Object.keys(CountryPhones).map(countryCapitals => {
      const phone = `+${CountryPhones[countryCapitals]}`;

      const country = countryCapitals
        .split('/')
        .map(cap => countryNames[cap])
        .join(' / ');

      const str = `(${phone}) ${country}`;
      if (!initialCountry) initialCountry = str;

      return (
        <Option key={str} title={`+${phone}`}>
          {str}
        </Option>
      );
    });

    this.setState({ initialCountry, CountryOptions });
  }

  getCode(prefix) {
    const {
      form: { getFieldsValue }
    } = this.props;

    let code = getFieldsValue([`${prefix}Code`])[`${prefix}Code`] || '';

    code = code.substr(code.indexOf('(') + 1);
    code = code.substr(0, code.indexOf(')'));

    return code;
  }

  handleSubmit = e => {
    e.preventDefault();

    const {
      form: { validateFields }
    } = this.props;

    validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  generatePhonePicker = ({ prefix, name }) => {
    const {
      form: { getFieldDecorator }
    } = this.props;

    const { initialCountry, CountryOptions } = this.state;

    return (
      <Row gutter={8}>
        <Col xs={24} md={8}>
          <Form.Item label={`${name} phone code`}>
            {getFieldDecorator(`${prefix}Code`, {
              rules: [{ required: true, message: 'Please input a country Code!' }],
              initialValue: initialCountry
            })(<Select showSearch>{CountryOptions}</Select>)}
          </Form.Item>
        </Col>

        <Col xs={24} md={16}>
          <Form.Item label={`${name} phone number`}>
            {getFieldDecorator(`${prefix}Phone`, {
              rules: [{ required: true, message: 'Please input a phone number!' }]
            })(<Input addonBefore={this.getCode(prefix)} placeholder={`${prefix} Phone`} />)}
          </Form.Item>
        </Col>
      </Row>
    );
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        {this.generatePhonePicker({ prefix: 'user', name: 'User' })}
        <Divider />
        {this.generatePhonePicker({ prefix: 'club', name: 'Club' })}

        <Form.Item>
          <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
            Register
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const RegisterForm = Form.create()(RegisterFormBase);

// eslint-disable-next-line react/no-multi-comp
const Register = memo(() => (
  <Row style={{ margin: 'auto 0px' }} align="middle" justify="center" type="flex" gutter={24}>
    <Col xs={24} md={12}>
      <Row>
        <Col xs={24} style={{ textAlign: 'center' }}>
          <img src={Logo} style={{ maxHeight: '300px' }} alt="Logo" />
        </Col>

        <Divider />

        <Col xs={24}>
          <RegisterForm />
        </Col>
      </Row>
    </Col>
  </Row>
));

export default Register;
