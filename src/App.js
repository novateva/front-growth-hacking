import React, { memo } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Layout } from 'antd';

import './styles/app.scss';

import { Register } from './Components';

const App = memo(() => (
  <BrowserRouter>
    <Layout id="app">
      <Register />
    </Layout>
  </BrowserRouter>
));

export default App;
